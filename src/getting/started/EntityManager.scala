package getting.started

import scala.util.Random

/**
 * Actual process layer
 */
object EntityManager {

  /**
   * Print out the entity. Use Random().nextBoolean() to determine whether an error object should be returned
   */
  def criticalProcess(entity: Entity): Either[EntityError, Entity] = ???

  /**
   * This method should not block the main flow, that means, you don't care about the result of this method. Even it's such
   * a big failure, it will print no error message
   * @param entity
   * @return
   */
  def nonCriticalProcess(entity: Entity): Either[EntityError, Entity] = ???

}

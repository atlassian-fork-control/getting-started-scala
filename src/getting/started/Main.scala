package getting.started

/**
 *
 * Sample should be
 *
 * 1. Retrieving an object from external system. It may or may not empty
 * 2. Validate new entity as it needs to
 *
 *
 */
object Main {
  def main(args: Array[String]): Unit = {
    val entity: Option[Entity] = EntityStore.getEntity
    val result: Either[EntityError, Entity] = EntityService.processEntity(entity)
    result.fold(
      error => println(s"Error message found ${error.toString}"),
      entity => println(s"Hello")
    )
  }
}


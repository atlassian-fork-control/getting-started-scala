**A simple piece of application that you can use to get started with Scala**

* `Main` contains main class to run the application
* `EntityStore` contains piece of code to generate new `Entity`.
* `EntityService` contains piece of code to validate any given `Entity`
* `EntityManager` contains piece of code to do all of magic with `Entity`
* `EntityError` contains all type of errors